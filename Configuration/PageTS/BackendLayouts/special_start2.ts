######################################
#### BACKENDLAYOUT: SPECIAL START ####
######################################
mod {
    web_layout {
        BackendLayouts {
            special_start2 {
                title = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:backend_layout.special_start2
                config {
                    backend_layout {
                        colCount = 3
                        rowCount = 5
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.border
                                        colPos = 3
                                        colspan = 3
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:backend_layout.column.top
                                        colPos = 4
                                        colspan = 3
                                    }
                                }
                            }
                            3 {
                                columns {
                                    1 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.middle1
                                        colPos = 20
                                    }
                                    2 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.middle2
                                        colPos = 21
                                    }
                                    3 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.middle3
                                        colPos = 22
                                    }
                                }
                            }
                            4 {
                                columns {
                                    1 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.normal
                                        colPos = 0
                                        colspan = 3
                                    }
                                }
                            }
                            5 {
                                columns {
                                    1 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.footer1
                                        colPos = 10
                                    }
                                    2 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.footer2
                                        colPos = 11
                                    }
                                    3 {
                                        name = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:backend_layout.column.footer3
                                        colPos = 12
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:st_t3core/Resources/Public/Images/BackendLayouts/special_start2.gif
            }
        }
    }
}
