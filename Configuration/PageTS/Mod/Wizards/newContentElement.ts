#########################
#### ShareTransition ####
#########################
mod.wizards.newContentElement.wizardItems {
	common {
		show = *
		elements >
		elements {
			header {
				iconIdentifier = content-header
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_headerOnly_description
				tt_content_defValues {
					CType = header
				}
			}
			text {
				iconIdentifier = content-text
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_regularText_description
				tt_content_defValues {
					CType = text
				}
			}
			textpic {
				iconIdentifier = content-textpic
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_description
				tt_content_defValues {
					CType = textpic
				}
			}
			image {
				iconIdentifier = content-st-t3core-image
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_description
				tt_content_defValues {
					CType = image
				}
			}
			external_media {
				iconIdentifier = content-bootstrappackage-externalmedia
				title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.external_media
				description = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.external_media.description
				tt_content_defValues {
					CType = external_media
				}
			}
			table {
				iconIdentifier = content-table
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_table_description
				tt_content_defValues {
					CType = table
				}
			}
			div {
				iconIdentifier = content-special-div
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_divider_description
				tt_content_defValues {
					CType = div
				}
			}
		}
	}
	text {
		after = common
	}
	media {
		after = common,text
        show = *
		elements >
		elements {
			textpic {
				iconIdentifier = content-textpic
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_textImage_description
				tt_content_defValues {
					CType = textpic
				}
			}
			image {
				iconIdentifier = content-st-t3core-image
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:common_imagesOnly_description
				tt_content_defValues {
					CType = image
				}
			}
			media {
				iconIdentifier = mimetypes-x-content-multimedia
				title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.media
				description = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.media.description
				tt_content_defValues {
					CType = media
				}
			}
			external_media {
				iconIdentifier = content-bootstrappackage-externalmedia
				title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.external_media
				description = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.external_media.description
				tt_content_defValues {
					CType = external_media
				}
			}
			collage {
				iconIdentifier = content-st-t3core-collage
				title = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.collage
				description = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.collage.description
				tt_content_defValues {
					CType = collage
				}
			}
			audio {
				iconIdentifier = content-audio
				title = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.audio
				description = LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:content_element.audio.description
				tt_content_defValues {
					CType = audio
				}
			}
			uploads {
				iconIdentifier = content-special-uploads
				title = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_title
				description = LLL:EXT:backend/Resources/Private/Language/locallang_db_new_content_el.xlf:special_filelinks_description
				tt_content_defValues {
					CType = uploads
				}
			}
		}
	}
	forms {
		elements {
			st_mailchimp {
				iconIdentifier = content-st-t3core-st_mailchimp
				title = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.st_mailchimp
				description = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.st_mailchimp.description
				tt_content_defValues {
					CType = st_mailchimp
				}
			}
		}
		show := addToList(st_mailchimp)
	}
	ShareTransition {
    	header = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:theme.name
		after = common,menu,special,forms,plugins,interactive,media,text
		show = *
    	elements {
			collage {
				iconIdentifier = content-st-t3core-collage
				title = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.collage
				description = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.collage.description
				tt_content_defValues {
					CType = collage
				}
			}
			st_mailchimp {
				iconIdentifier = content-st-t3core-st_mailchimp
				title = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.st_mailchimp
				description = LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.st_mailchimp.description
				tt_content_defValues {
					CType = st_mailchimp
				}
			}
		}
	}
}
