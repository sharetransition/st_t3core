#############
#### RTE ####
#############
RTE {
    config {
        tt_content {
            bodytext {
                types {
                    text {
                    	preset = st_t3core
                    }
                    textcolumn {
                        preset = st_t3core
                    }
                    texticon {
                        preset = st_t3core
                    }
                    textmedia {
                        preset = st_t3core
                    }
                    textpic {
                        preset = st_t3core
                    }
                    textteaser {
                        preset = st_t3core
                    }
                    panel {
                        preset = st_t3core
                    }
                }
            }
        }
        tx_bootstrappackage_accordion_item {
            bodytext {
                preset = st_t3core
            }
        }
        tx_bootstrappackage_tab_item {
            bodytext {
                preset = st_t3core
            }
        }
    }
}
