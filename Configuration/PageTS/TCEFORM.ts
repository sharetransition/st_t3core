# This file adds page TS config and is included in ext_localconf.php.

TCAdefaults {
    tt_content {
        # Default no frame (fixed in TCA column overrides instead)
        # frame_class = none

        # Set default image above left
        imageorient = 10

        # Default responsive width 100%
        figurewidthresponsive = 100
    }

    # BOOTSTRAP
    tx_bootstrappackage_carousel_item {
        item_type = backgroundimage
    }
}

# PAGES
TCEFORM {
    pages {
        layout.disabled = 1
    }
}



// NOBODY wants or should edit this # Suggested from Anatomy of sitepackages presentation (see general info setup.ts)
// really, let them disappear
TCEFORM {
    tt_content {
        table_bgColor.disabled = 1
        table_border.disabled = 1
        table_cellspacing.disabled = 1
        table_cellpadding.disabled = 1
        pi_flexform.table.sDEF {
            acctables_nostyles.disabled = 1
            acctables_tableclass.disabled = 1
        }
    }
}

# CUSTOM
TCEFORM {
        # Disabling backend options
        date.disabled = 1
        sys_language_uid.disabled = 1
        fe_group.disabled = 1
        editlock.disabled = 1
        categories.disabled = 1
        layout.disabled = 1
        linkToTop.disabled = 1
}

# DISABLED CTYPES
TCEFORM {
    tt_content {
        CType.removeItems = carousel_small, carousel_fullscreen, bullets, menu_categorized_content, menu_categorized_pages, menu_subpages, menu_recently_updated, menu_related_pages, menu_section, menu_section_pages, menu_thumbnail_list, menu_thumbnail_dir
    }
}

# IMAGEORIENT
TCEFORM {
    tt_content {
        imageorient {
            # Enable all, but still disabled default many elements
            removeItems >
            types {
                image {
                    disabled = 0
                    removeItems = 8,9,10,17,18,25,26

                }
                media {
                    disabled = 0
                    //removeItems = 8,9,10,17,18,25,26
                }
                collage {
                    //removeItems = 8,9,10,17,18,25,26
                }
                text {
                    disabled = 0
                    removeItems = 8,9,10,17,18,25,26
                }
                textcolumn {
                    disabled = 0
                    removeItems = 8,9,10,17,18,25,26
                }
                teaser {
                    disabled = 0
                    removeItems = 8,9,10,17,18,25,26
                }
            }
        }
    }
}

# MORE COLUMNS
TCEFORM {
    tt_content {
        figurewidthresponsive {
            removeItems = 8, 17
        }
        column_number {
            removeItems = 4
        }
        imagewidth {
            disabled = 1
            types {
                image {
                    disabled = 0
                }
                textpic {
                    disabled = 0
                }
                collage {
                    disabled = 0
                }
            }
        }
        imageheight.disabled = 1
        imagecols {
            disabled = 1
            types {
                image {
                    disabled = 0
                }
                textpic {
                    disabled = 0
                }
                collage {
                    disabled = 0
                }
            }
        }
        figurecolsep {
            disabled = 1
            types {
                collage {
                    disabled = 0
                }
            }
        }
        figurerowsep {
            disabled = 1
            types {
                collage {
                    disabled = 0
                }
            }
        }
    }
}

# NEWS
# Disable tag plugin
TCEFORM.tt_content.pi_flexform.news_pi1.sDEF.switchableControllerActions.removeItems = Tag->list
TCEFORM.tx_news_domain_model_news {
    # remove tags
    tags {
        disabled = 1
    }
    # Remove metadata tag entirely
    keywords {
        disabled = 1
    }
    description {
        disabled = 1
    }
    alternative_title {
        disabled = 1
    }
    path_segment {
        disabled = 1
    }
}
