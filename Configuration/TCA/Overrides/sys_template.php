<?php
defined('TYPO3_MODE') || die();

/***************
 * Default TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'st_t3core',                   // Extension Key
    'Configuration/TypoScript', // Path to setup.ts and constants.ts
    'ShareTransition T3Core'           // Title in the selector box
);


// Blog config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('st_t3core', 'Configuration/TypoScript/Ext/Blog', 'ST Blog Config');

// News config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('st_t3core', 'Configuration/TypoScript/Ext/News', 'ST News Config');

// Shop config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('st_t3core', 'Configuration/TypoScript/Ext/Shop', 'ST Shop Config');
