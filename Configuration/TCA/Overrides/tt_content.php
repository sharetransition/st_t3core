<?php
defined('TYPO3_MODE') || die();

/***************
 * Add content element group to seletor list
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:theme.name',
        '--div--'
    ],
    '--div--',
    'before'
);
