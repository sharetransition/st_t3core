<?php
defined('TYPO3_MODE') || die();

/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['st_mailchimp'])) {
    $GLOBALS['TCA']['tt_content']['types']['st_mailchimp'] = [];
}

/***************
 * Add content element to selector list
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.element.st_mailchimp',
        'st_mailchimp',
        'content-st-t3core-st_mailchimp'
    ],
    '--div--',
    'after'
);


/***************
 * Assign Icon
 */
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['st_mailchimp'] = 'content-st-t3core-st_mailchimp';

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['types']['st_mailchimp'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['st_mailchimp'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                st_mailchimpurl,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                 additionaltext,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
				--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
                --palette--;LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.palettes.mediaadjustments;mediaAdjustments,
                --palette--;LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.palettes.gallerysettings;gallerySettings,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
    ]
);
