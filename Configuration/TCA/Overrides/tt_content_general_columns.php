<?php
defined('TYPO3_MODE') || die();

// Add mailchimp url input
$GLOBALS['TCA']['tt_content']['columns']['st_mailchimpurl'] = [
    'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.st_mailchimpurl',
	'config' => [
		'type' => 'input',
		'eval' => 'nospace,lower,required'
	]
];

// Remove default frame put no frame on top
$GLOBALS['TCA']['tt_content']['columns']['frame_class'] = [
	'exclude' => true,
	'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class',
	'config' => [
		'type' => 'select',
		'renderType' => 'selectSingle',
		'items' => [
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.none', 'none'],
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.ruler_before', 'ruler-before'],
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.ruler_after', 'ruler-after'],
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.indent', 'indent'],
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.indent_left', 'indent-left'],
			['LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:frame_class.indent_right', 'indent-right'],
			['Well', 'well'],
			['Jumbotron', 'jumbotron'],
		],
	'default' => 'none'
	]
];



// Give teaser richtext
$GLOBALS['TCA']['tt_content']['columns']['teaser'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns']['teaser'],
    [
		'config' => [
			'enableRichtext' => true,
			'richtextConfiguration' => 'st_t3core'
		]
    ]
);

// Add additionaltext same as bodytext
$GLOBALS['TCA']['tt_content']['columns']['additionaltext'] = [
    'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.additionaltext',
	'l10n_mode' => 'prefixLangTitle',
	'config' => [
		'enableRichtext' => true,
		'richtextConfiguration' => 'st_t3core',
		'type' => 'text',
		'cols' => '80',
		'rows' => '15',
		'softref' => 'typolink_tag,images,email[subst],url',
		'search' => [
			'andWhere' => 'CType=\'text\' OR CType=\'textpic\' OR CType=\'textmedia\''
		]
	]
];


// Add responsive figure width TCA
$GLOBALS['TCA']['tt_content']['columns']['figurewidthresponsive'] = [
	'exclude' => 0,
	'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.figurewidthresponsive',
	'config' => [
		'type' => 'select',
		'renderType' => 'selectSingle',
		'items' => [
			[
				'100',
				100
			],
			[
				'92',
				92
			],
			[
				'83',
				83
			],
			[
				'75',
				75
			],
			[
				'66',
				66
			],
			[
				'58',
				58
			],
			[
				'50',
				50
			],
			[
				'42',
				42
			],
			[
				'33',
				33
			],
			[
				'25',
				25
			],
			[
				'17',
				17
			],
			[
				'8',
				8
			]
		],
		'default' => 100
	],
];

// Add text column number
$GLOBALS['TCA']['tt_content']['columns']['column_number'] = [
	'exclude' => true,
	'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.column_number',
	'config' => [
		'type' => 'select',
		'renderType' => 'selectSingle',
		'items' => [
			[
				'2',
				2
			],
			[
				'3',
				3
			],
			[
				'4',
				4
			]
		],
		'default' => 2
	],
];


// Re-label and set range for static width
$GLOBALS['TCA']['tt_content']['columns']['imagewidth'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns']['imagewidth'],
	[
		'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.imagewidth',
		'config' => [
			'eval' => 'int,null',
			'range' => [
				'lower' => '0',
				'upper' => '200'
			],
			'default' => null
		]
	]
);

// Add figurerowsep for collage
$GLOBALS['TCA']['tt_content']['columns']['figurerowsep'] = [
	'exclude' => true,
	'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.figurerowsep',
	'config' => [
		'type' => 'input',
		'size' => 4,
		'max' => 4,
		'eval' => 'int,null',
		'range' => [
			'upper' => '100',
			'lower' => '0',
		],
		'default' => null,
	]
];
// Add figurecolsep for collage
$GLOBALS['TCA']['tt_content']['columns']['figurecolsep'] = [
	'exclud' => true,
	'label' => 'LLL:EXT:st_t3core/Resources/Private/Language/Backend.xlf:content.columns.figurecolsep',
	'config' => [
		'type' => 'input',
		'size' => 4,
		'max' => 4,
		'eval' => 'int,null',
		'range' => [
			'upper' => '100',
			'lower' => '0',
		],
		'default' => null,
	]
];
