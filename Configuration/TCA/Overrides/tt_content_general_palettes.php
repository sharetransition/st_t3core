<?php
defined('TYPO3_MODE') || die();

$GLOBALS['TCA']['tt_content']['palettes']['mediaAdjustments'] = array_replace_recursive (
	$GLOBALS['TCA']['tt_content']['palettes']['mediaAdjustments'],
	[
	'showitem' => '
		figurewidthresponsive,
		imagewidth,
		figurerowsep,
		figurecolsep,
		imageheight,
		imageborder
		',
	]
);

