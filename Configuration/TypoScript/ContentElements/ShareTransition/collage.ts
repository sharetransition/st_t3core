########################
#### CTYPE: COLLAGE ####
########################

tt_content.collage >
tt_content.collage =< lib.contentElement
tt_content.collage {

    ################
    ### TEMPLATE ###
    ################
    templateName = ShareTransition/Collage

    ##########################
    ### DATA PREPROCESSING ###
    ##########################
    dataProcessing {
        10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
        10 {
            references.fieldName = image
        }
    }
}
