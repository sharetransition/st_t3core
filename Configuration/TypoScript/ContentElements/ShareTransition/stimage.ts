########################
#### CTYPE: stimage ####
########################

tt_content.stimage >
tt_content.stimage =< lib.contentElement
tt_content.stimage {

    ################
    ### TEMPLATE ###
    ################
    templateName = Figure

    ##########################
    ### DATA PREPROCESSING ###
    ##########################

    dataProcessing {
        10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
        10 {
            references.fieldName = image
        }
    }
}
