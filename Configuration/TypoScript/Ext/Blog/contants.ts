## DEPS
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:blog/Configuration/TypoScript/Static/constants.txt">

## TEMPLATES
plugin.tx_blog.view {
    templateRootPath = EXT:st_t3core/Resources/Private/Templates/Blog
    partialRootPath = EXT:st_t3core/Resources/Private/Partials/Blog
    layoutRootPath = EXT:st_t3core/Resources/Private/Layouts/Blog
}
