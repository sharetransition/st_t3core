## DEPS
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:blog/Configuration/TypoScript/Static/setup.txt">

## TEMPLATES
# Predefined 0 = EXT:blog/Resources/Private/Templates
# 10 = st_t3core; 20 will be set in site package constants
plugin.tx_blog.view {
	templateRootPaths {
		10 = EXT:st_t3core/Resources/Private/Templates/Blog/
		20 = {$plugin.blog.view.templateRootPath}
	}
	partialRootPaths {
        10 = EXT:st_t3core/Resources/Private/Partials/Blog/
		20 = {$plugin.blog.view.partialRootPath}
	}
	layoutRootPaths {
        10 = EXT:st_t3core/Resources/Private/Layouts/Blog/
		20 = {$plugin.blog.view.layoutRootPath}
	}
}

## CONFIG
plugin.tx_blog.settings {
	sharing.enabled = 1
	comments.active = 1
	# custom
	comments.email = 1
	comments.emailRequired = 0
	comments.url = 0
}

config.tx_extbase {
    objects {
        # Extending a controller
        T3G\AgencyPack\Blog\Domain\Validator\CommentValidator {
		className = ShareTransition\StT3Core\Blog\Domain\Validator\CommentValidator
	}
    }
}

# sidebarWidgets {
#    10 = tt_content.list.20.blog_recentpostswidget
#    20 = tt_content.list.20.blog_categorywidget
#    30 = tt_content.list.20.blog_tagwidget
#    40 = tt_content.list.20.blog_commentswidget
#    50 = tt_content.list.20.blog_archivewidget
# }

