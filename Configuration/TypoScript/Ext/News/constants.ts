## DEPS
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/constants.txt">
# News bootstrap
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/Styles/Twb/constants.txt">

## TEMPLATES
plugin.tx_news.view.twb {
    templateRootPath = EXT:st_t3core/Resources/Private/Templates/NewsTwb/
    partialRootPath = EXT:st_t3core/Resources/Private/Partials/NewsTwb/
    layoutRootPath = EXT:st_t3core/Resources/Private/Layouts/NewsTwb/
}
