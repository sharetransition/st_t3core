## TYPOSCRIPT
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/setup.txt">
# News bootstrap
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/Styles/Twb/setup.txt">


## TEMPLATE
# Predefined 0 = EXT:news/Resources/Private/Templates
# And news bootstrap, 1 = EXT:news/Resources/Private/Templates/Styles/Twb/Templates
# 10 = st_t3core; 20 will be set in site package constants
plugin.tx_news.view {
	templateRootPaths {
		10 = EXT:st_t3core/Resources/Private/Templates/NewsTwb/
		20 = {$plugin.tx_news.view.twb.templateRootPath}
	}
	partialRootPaths {
        10 = EXT:st_t3core/Resources/Private/Partials/NewsTwb/
		20 = {$plugin.tx_news.view.twb.partialRootPath}
	}
	layoutRootPaths {
        10 = EXT:st_t3core/Resources/Private/Layouts/NewsTwb/
		20 = {$plugin.tx_news.view.twb.layoutRootPath}
	}
}

## CONFIG
# The news plugin also require a detailed configuration with specific page ID's in realurl_conf.php describing the plugin url rewriting.
# Add news date to detail url
plugin.tx_news.settings {
    recursive = 5
    # cropMaxCharacters = 120
    link {
        hrDate = 1
    }
    list {
        paginate {
            insertAbove = 0
            itemsPerPage = 6
        }
        media {
            dummyImage = EXT:st_t3core/Resources/Public/Images/no-image.jpg
        }
    }
}
