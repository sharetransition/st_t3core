plugin.bootstrap_package.settings.less {

// navbar
navbar-height-lg = 100px
logo-height-lg = 60px
logo-height-sm = 34px
// For relative heights you can use:
// logo-lg-height = calc(0.4 * navbar-lg-height);

// for forms
input-text-color = #555

// footer

footer-bottom-bg = darken(@footer-bg, 4%)

// BOOTSTRAP VARIABLES
// add xxs
screen-xxs-max = (@screen-xs - 1)

grid-gutter-width = 30px
figure-margin = 1em
brand-primary = #585656

dropdown-link-hover-bg = lighten(@brand-primary, 15%)
// link-color =            #1a0dab



//== Typography, mostly imported from bootstrap
//
//## Font, line-height, and color for body text, headings, and more.

//@font-family-sans-serif:  "Helvetica Neue", Helvetica, Arial, sans-serif;
font-family-serif =        Georgia, "Times New Roman", Times, serif
//** Default monospace fonts for `<code>`, `<kbd>`, and `<pre>`.
font-family-monospace =    Menlo, Monaco, Consolas, "Courier New", monospace
font-family-base =         @font-family-sans-serif
font-size-base =           16px
font-size-large =          ceil((@font-size-base * 1.25))
font-size-small =          ceil((@font-size-base * 0.85))
font-size-h1 =             floor((@font-size-base * 2.6))
font-size-h2 =             floor((@font-size-base * 2))
font-size-h3 =             ceil((@font-size-base * 1.6))
font-size-h4 =             ceil((@font-size-base * 1.25))
font-size-h5 =             @font-size-base
font-size-h6 =             ceil((@font-size-base * 0.85))

h1-margin-top =           floor((@font-size-h1 * 1))
h1-margin-bot =           floor((@font-size-h1 * 0.5))
h2-margin-top =           floor((@font-size-h2 * 1))
h2-margin-bot =           floor((@font-size-h2 * 0.5))
h3-margin-top =           floor((@font-size-h3 * 1))
h3-margin-bot =           floor((@font-size-h3 * 0.5))
h4-margin-top =           floor((@font-size-h4 * 1))
h4-margin-bot =           floor((@font-size-h4 * 0.5))
h5-margin-top =           floor((@font-size-h5 * 1))
h5-margin-bot =           floor((@font-size-h5 * 0.5))
h6-margin-top =           floor((@font-size-h6 * 1))
h6-margin-bot =           floor((@font-size-h6 * 0.5))

//** Unit-less `line-height` for use in components like buttons.
// 20/14
line-height-base =         1.428571429
//** Computed "line-height" (`font-size` * `line-height`) for use with `margin`, `padding`, etc. ~20px
line-height-computed =     floor((@font-size-base * @line-height-base))

//** By default, this inherits from the `<body>`.
headings-font-family =     inherit
headings-font-weight =     300
headings-line-height =     1.1
headings-color =           inherit

}
