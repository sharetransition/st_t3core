####################
### DEPENDENCIES ###
####################
## BOOTSTRAP
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:bootstrap_package/Configuration/TypoScript/constants.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/constants-less.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/Ext/Blog/constants.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/Ext/News/constants.ts">
## CAPTCHA
#<INCLUDE_TYPOSCRIPT: source="FILE:EXT:sr_freecap/Configuration/TypoScript/constants.txt">


###########################
### TEMPLATE ROOT PATHS ###
###########################
# As defined in st_t3core setup.ts; If the specified template isn't found in these prioritized paths;
# first fall back to look in st_t3core; final fall back to bootstrap package.
## PAGE
page.fluidtemplate {
    layoutRootPath = EXT:st_t3core/Resources/Private/Layouts/Page/
    partialRootPath = EXT:st_t3core/Resources/Private/Partials/Page/
    templateRootPath = EXT:st_t3core/Resources/Private/Templates/Page/
}
## BOOTSTRAP CONTENT ELEMENTS
plugin.bootstrap_package_contentelements {
    view {
        layoutRootPath = EXT:st_sitepack/Resources/Private/Layouts/ContentElements/
        partialRootPath = EXT:st_sitepack/Resources/Private/Partials/ContentElements/
        templateRootPath = EXT:st_sitepack/Resources/Private/Templates/ContentElements/
    }
}


######################
### PAGE VARIABLES ###
######################
page {
    theme {
        language {
            enable = 0
        }
        copyright {
            text = Welcome to the sharetransition web platform
        }
    }
    logo {
        file = EXT:st_t3core/Resources/Public/Images/st-logo-lg.png
        linktitle = sharetransition
    }
    logoSmallScreen {
        file = EXT:st_t3core/Resources/Public/Images/st-logo-sm.png
    }
    favicon {
        file = EXT:st_t3core/Resources/Public/Icons/favicon.png
    }
}


#####################
### EXT VARIABLES ###
#####################
## BOOTSTRAP
# Disallow bootstrap variables to overwrite less
plugin.bootstrap_package.settings {
    overrideLessVariables = 1
}
