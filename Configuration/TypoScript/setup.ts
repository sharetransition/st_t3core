###########################
### GLOBAL DEPENDENCIES ###
###########################
## BOOTSTRAP
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:bootstrap_package/Configuration/TypoScript/setup.txt">
## FRONT END EDITING
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:frontend_editing/Configuration/TypoScript/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/Ext/News/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/Ext/Blog/setup.ts">


## CONTENT ELEMENTS
# Modifications
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/ContentElements/content_elements_modified.ts">
# New content elements setup are set up in ext_localconf.php
# ShareTransition
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/ContentElements/ShareTransition/collage.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/ContentElements/ShareTransition/mailchimp.ts">

#<INCLUDE_TYPOSCRIPT: source="FILE:EXT:st_t3core/Configuration/TypoScript/ContentElements/ShareTransition/card.ts">
## CAPTCHA
#<INCLUDE_TYPOSCRIPT: source="FILE:EXT:sr_freecap/Configuration/TypoScript/setup.txt">



######################
### STYLESHEETS/JS ###
######################
# Exclude bootstrap package styling as it is imported again in this core package main.less.
# The core main.less is then overwritten and imported in the sitepackages main.less.
# In this way all less variables can be used without multiple loading.
page.includeCSS.theme >
page.includeCSS.main = EXT:st_t3core/Resources/Public/Stylesheets/main.less


## FUNCTIONS
# dotdotdot = crop text and add ... and after-element
page.includeJSFooterlibs.jquery_dotdotdot = EXT:st_t3core/Resources/Public/JavaScript/Libs/jquery.dotdotdot.min.js
# flip cards
page.includeJSFooterlibs.flip = EXT:st_t3core/Resources/Public/JavaScript/Libs/jquery.flip.min.js
# smart resize debounce = exec every 100 ms
page.includeJSFooterlibs.smart_resize = EXT:st_t3core/Resources/Public/JavaScript/Libs/jquery.smart-resize.js

## CAROUSEL
page.includeJSFooter.carousel = EXT:st_t3core/Resources/Public/JavaScript/carousel.js
# page.includeCSS.carousel = EXT:st_t3core/Resources/Public/Less/carousel.less

## CARDS
page.includeJSFooter.card_flip = EXT:st_t3core/Resources/Public/JavaScript/flip-cards.js
# Grid
page.includeJSFooter.card_grid = EXT:st_t3core/Resources/Public/JavaScript/card-grid.js
## Dotdotdot implementation
page.includeJSFooter.dotdotdot = EXT:st_t3core/Resources/Public/JavaScript/dotdotdot.js


## CUSTOM OVERLAYS
page.includeJSFooter.overlay = EXT:st_t3core/Resources/Public/JavaScript/overlay.js

## WAVES EFFECT
page.includeJSFooterlibs.waves = EXT:st_t3core/Resources/Public/JavaScript/Libs/waves.min.js
page.includeJSFooter.wave_init = EXT:st_t3core/Resources/Public/JavaScript/waves.js

## FIGURE FIXES
# Static width fix
page.includeJSFooter.figure_static = EXT:st_t3core/Resources/Public/JavaScript/figure-static.js
# Collage rendering fix
page.includeJSFooter.collage = EXT:st_t3core/Resources/Public/JavaScript/ContentElements/collage.js

# page.includeCSS.cards = EXT:st_t3core/Resources/Public/Less/cards.less
# page.includeCSS.cards_rotate = EXT:st_t3core/Resources/Public/Less/cards-rotate.less


######################
### FLUID TEMPLATE ###
######################
# page.10 = bootstrap package defined fluid template
page.10 {
    settings {
        # Add new logo for small screens included in the custom nav main partial
        logoSmallScreen {
            file = {$page.logoSmallScreen.file}
            fileInverted = {$page.logoSmallScreen.fileInverted}
        }
    }
    dataProcessing {
        # Breadcrumb exclude root page
        30 {
            special.range = 1|-1
            # Exclude category page
            # excludeUidList = 29
        }
    }
}

#######################
### BACKEND LAYOUTS ###
#######################
# BackendLayouts are included from directory PageTS/BackendLayouts in ext.localconf.php
# If created custom templates aren't simply overwriting extension templates with the same name,
# they have to be assigned here. Template root paths are defined in their section below.
page.10.templateName.stdWrap.cObject {
        pagets__special_start2 = TEXT
        pagets__special_start2.value = SpecialStart2
}

###########################
### TEMPLATE ROOT PATHS ###
###########################
# Template paths searched through from bottom to top

## PAGE
# The fluid template paths
# Predefined 0 = EXT:bootstrap_package/Resources/Private/Templates/Page/
# 10 = st_t3core; 20 will be set in site package constants
page.10 {
    templateRootPaths {
        10 = EXT:st_t3core/Resources/Private/Templates/Page/
        20 = {$page.fluidtemplate.templateRootPath}
    }
    partialRootPaths {
        10 = EXT:st_t3core/Resources/Private/Partials/Page/
        20 = {$page.fluidtemplate.partialRootPath}
    }
    layoutRootPaths {
        10 = EXT:st_t3core/Resources/Private/Layouts/Page/
        20 = {$page.fluidtemplate.layoutRootPath}
    }
}

## BOOTSTRAP CONTENT ELEMENTS
# Predefined 0 = EXT:bootstrap_package/Resources/Private/Templates/ContentElements/
# 10 = st_t3core; 20 will be set in site package constants
lib.contentElement {
    templateRootPaths {
        10 = EXT:st_t3core/Resources/Private/Templates/ContentElements/
        20 = {$page.fluidtemplate.templateRootPath}
    }
    partialRootPaths {
        10 = EXT:st_t3core/Resources/Private/Partials/ContentElements/
        20 = {$page.fluidtemplate.templateRootPath}
    }
    layoutRootPaths {
        10 = EXT:st_t3core/Resources/Private/Layouts/ContentElements/
        20 = {$page.fluidtemplate.templateRootPath}
    }
}


###############
### REALURL ###
###############
# E.g www.site.com/home instead of url based on page ID. Requires the typo3 installation root .htaccess file (provided by default in my installation)
# and the apache configuration AllowOverride = All. Instead of using .htaccess files, all settings can also be applied directly in the apache configs.
config.absRefPrefix = /
config.simulateStaticDocuments = 0
config.tx_realurl_enable = 1
# Set the allowed range, "This is very important to prevent that the cache system gets flooded with forged values."
config.linkVars = L(0-3)


#################
### EXT SETUP ###
#################

## FRONT END EDITING - Version 8 only
# Enable front end editing, also has to be enabled for each user in the typo3 backend.
config.frontend_editing = 1
