# README

## ShareTransition
A demonstration and overview of the package is available at the ShareTransition Platform <http://sharetransition.com>.

## Description
The ShareTransition TYPO3 Core configuration package is inspired by and extending upon the TYPO3 official distribution's *bootstrap_package*, <http://www.bootstrap-package.com/>.

Almost all content elements as well as the news system have been given a more material feel in terms of design and a range of responsive sizing and alignment options have been implemented to empower backend users. Some new content elements have also been added, and the backend has also been somewhat styled and cleaned up to create a more intuitive workflow.

## Some typoscript contants
```
page {
    theme {
        copyright {
            text = Welcome to the ShareTransition TYPO3 Core
        }
    }
    logo {
        file = FILEADMIN:path/to/logo.png
    }
    # set custom core small logo
    logoSmallScreen {
        file = FILEADMIN:path/to/logo-sm.png
    }
    favicon {
        file = FILEADMIN:path/to/favicon.png
    }
}

plugin.bootstrap_package.settings.less {
	navbar-height-lg = 120px
	logo-height-lg = 50px
	logo-height-sm = 35px
	brand-primary = #585656
}
```
