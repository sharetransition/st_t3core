// Trigger resize event to assure proper rendering of collage
window.onload = function() {
  if ($('.collage').length) {
    $(window).trigger('resize');
  };
}
