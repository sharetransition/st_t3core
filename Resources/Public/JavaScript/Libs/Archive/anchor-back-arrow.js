// Back to top arrow fade at anchor element offset
$(document).ready(function(){
    var targetOffset = $('#anchor-point').offset().top;
    var $w = $(window).scroll(function(){
        if ( $w.scrollTop() > targetOffset ) {
            $('#back-arrow').fadeIn();
        } else {
            $('#back-arrow').fadeOut();
        }
    });
});
