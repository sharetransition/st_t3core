var newsItems = $(".news-list-view .news-list-item");

var newsLength = newsItems.length;
// if exist news
if (newsLength) {
    // ### EXECUTE ###
    // first grid
    cardGrid();
    // smart redraw on resize with 0.1s response delay
    $(window).smartresize(function(){
        cardGrid();
    });
    // ### FUNCTIONS ###
    // return nr of cols
    function colNr(){
        // get width in % of parent
        var itemWidth = ( 100 * parseFloat(newsItems.width()) / parseFloat(newsItems.parent().width()) );
        if (itemWidth > 0  && itemWidth < 34) {return 3}
        else if (itemWidth >= 34 && itemWidth <= 50) {return 2}
        else {return 1}
    }

    // swap arrow triangle after class top/bot, hoverable-top/bot
    // and image-content up/down
    function cardSwap(n, swap) {
        content = newsItems.eq(n-1).children(".card-content");
        image = newsItems.eq(n-1).children(".card-image");

        // swap if swap = true and image-below
        if (swap) {
            if (content.hasClass("image-below")) {
                content.insertAfter(image);
                content.toggleClass("image-below image-above");
                image.toggleClass("hoverable hoverable-top");
            }
        } else { // unswap if swap = false and swapped
                if (content.hasClass("image-above")) {
                    image.insertAfter(content);
                    content.toggleClass("image-below image-above");
                    image.toggleClass("hoverable hoverable-top");
                }
            }
        }
    // reset cards
    // function cardReset(n) {
    //     content = newsItems.eq(n-1).children(".card-content");
    //     image = newsItems.eq(n-1).children(".card-image");
    //
    //     }
    // }
    // create grid by swapping cards
    function cardGrid() {
        cols = colNr();
        for (var n = 1; n <= newsLength; n++) {

            switch (cols) {
                case 1:
                    cardSwap(n, false);
                    break;

                case 2:
                    // swap even nr.
                    if ( n % 2 === 0 ) {
                        cardSwap(n, true);
                    }
                    else {
                        cardSwap(n, false);
                    } break;

                case 3:
                    // swap 3*n-1
                    if ( n % 3 === 2 ) {
                        // middle card
                        cardSwap(n, true);
                    } else {
                        cardSwap(n, false);
                    } break;

            }
        }
    }
}
