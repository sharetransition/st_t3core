// Add carousel overlay of equal size
var carousels = $(".carousel");
carousels.each(function() {

var carousel = $(this);
var carouselInner = carousel.children(".carousel-inner");

    // create href overlay if item href
    href = carouselInner.children(".active").children("a").attr("href");
    if (typeof href !== typeof undefined && href !== false) {
        carouselInner.before('<a class="overlay inserted mask bg-primary waves-effect"></a>');
        var overlayMask = carouselInner.prev();
        overlayMask.attr("href", href);
    }
    // make hoverable if more than one item
    items = carouselInner.children(".item");
    if (items.length > 1) {
        carousel.addClass("hoverable");
    }
});
