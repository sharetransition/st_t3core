// run once on load if there are static elements on page
$(window).on('load',function() {
	var staticFigures = $(".figure-items-static");
	var figuresLength = staticFigures.length;

	if (figuresLength) {
		// Detach from text in wrap if static width larger than wrapThreshold
		// and smaller than xsThreshold,
		// Column always detaches below xsThreshold
		// Also always detach when reducing more items than itemsThreshold
		wrapThreshold = 100; // Maximum image width for allow wrap under xsThreshold
		textThreshold = 300; // Minimum text column width
		xsThreshold = 480; // Important to be set to @screen-xs to comply with styling for columns
		figureMargin = '1em'; // Set top and bot when detach
		itemsThreshold = 4 // Detach if trying to reduce more items than thresh
		staticSizer();
		window.dispatchEvent(new Event('resize'));
		// rerun on resize
		$(window).smartresize(function(){
			staticSizer();
		});
	}


	// MAIN - Set size of static figure and text
	function staticSizer(){
		for (var n = 0; n < figuresLength; n++) {
			// Get static items container, parent and text container
			var staticItems = $(staticFigures[n]).find(".collage-items-static, .gallery-row");
			var staticParent = $(staticFigures[n]).parent();
			var staticText = staticParent.find(".figure-text");
			// Individual Items and images in selected figure
			var staticItem = $(staticItems).find(".gallery-item, .collage-item");
			var staticImage = $(staticItems).find(".image").find("img");

			// Single image and parent (including text) width
			var imageWidth = $(staticImage).width();
			var parentWidth = $(staticParent).width();

			// Item type
			if ($(staticItems).hasClass("collage-items")) {
				var itemType = "collage";
			} else if ($(staticItems).hasClass("gallery-row")) {
				var itemType = "figure";
			}


			// COLLAGE - Set static sizes
			if ( itemType == "collage" ) {
				// Get initial column number and gap for figures
				colNr = $(staticItems).attr('class').split(" ")[2].split("-")[2];
				colGap = $(staticItems).css('column-gap').replace(/[^-\d\.]/g, '');
				// Initial newItemsWidth
				newItemsWidth = newCollageWidthFunc(imageWidth, colNr, colGap);
			}

			// FIGURE - Set static sizes
			else if ( itemType == "figure" ) {
				// Initial column number and gap for figures
				colNr = $(staticItem).attr('class').split(" ")[2].split("-")[3];
				colGap = 2 * $(staticItem).css('padding-left').replace(/[^-\d\.]/g, '');
				// Initial newItemsWidth
				newItemsWidth = newFigureWidthFunc(imageWidth, colNr, colGap);
			}

			// Check for responsive changes
			newItemsWidth = responsiveColumns(staticItems, staticItem, itemType, newItemsWidth, imageWidth, parentWidth, colNr, colGap);
			// Set final newItemsWidth
			staticItems.width(newItemsWidth);
			// Set text width if not small screen detach and in column
			if (!(smallScreenDetach(staticItems, staticItem, itemType, staticText, staticParent, imageWidth, newItemsWidth, parentWidth)) && $(staticText).hasClass("figure-text-column")) {
				setTextWidth(staticItems, itemType, staticText, newItemsWidth, colGap);
			}
		}
		return
	}


	// COLLAGE new figure width function
	function newCollageWidthFunc (imageWidth, colNr, colGap) {
		return newFigureWidth = imageWidth * colNr + ((colNr - 1) * colGap);
	}

	// FIGURE new figure width function
	function newFigureWidthFunc(imageWidth, colNr, colGap) {
		return newFigureWidth = colNr * (imageWidth + colGap);
	}


	// Responsive columns
	function responsiveColumns(staticItems, staticItem, itemType, newItemsWidth, imageWidth, parentWidth, colNr, colGap) {
		// Reduce columns if the figure is wrapping or column and has less space than parent minus textThreshold OR if not wrapping and column but less than parent
		if ( (newItemsWidth > (parentWidth - textThreshold) && ($(staticItems).parent().hasClass("figure-items-wrap") || $(staticItems).parent().hasClass("figure-items-column"))) ||
			newItemsWidth > parentWidth) {
			newItemsWidth = columnReducer(staticItems, staticItem, itemType, newItemsWidth, imageWidth, colNr);
			// Run again to check for further change
		if ( (newItemsWidth[0] > (parentWidth - textThreshold) && ($(staticItems).parent().hasClass("figure-items-wrap") || $(staticItems).parent().hasClass("figure-items-column"))) ||
			newItemsWidth[0] > parentWidth) {
				newItemsWidth = columnReducer(staticItems, staticItem, itemType, newItemsWidth[0], imageWidth, newItemsWidth[1]);
			}
		}
		// Else reset columnreducer inline css
		else {
			if ($(staticItem).css('width')) {
				$(staticItem).css('width', '');
				return newFigureWidthFunc(imageWidth, colNr, colGap);
			}
			else if ($(staticItems).css('column-count')) {
				$(staticItems).css({'-moz-column-count' : '', '-webkit-column-count' : '','column-count' : ''});
				return newCollageWidthFunc(imageWidth, colNr, colGap);
			}
		}
		return newItemsWidth[0]
	}

	// Column reducer
	function columnReducer(staticItems, staticItem, itemType, newItemsWidth, imageWidth, colNr) {
		switch(parseInt(colNr)) {
			case 6: // 6 to 3 col
				colNr = 3;
				if ( itemType == "figure" ) { // Figure
					$(staticItem).css('width', '33.33333%');
				} else if (itemType = "collage") { // Collage
					$(staticItems).css({'-moz-column-count' : '3', '-webkit-column-count' : '3','column-count' : '3'});
				}
				break;
			case 4: // 4 to 2 col
				colNr = 2;
				if ( itemType == "figure" ) { // Figure
					$(staticItem).css('width', '50%');
				} else if ( itemType == "collage" ) { // Collage
					$(staticItems).css({'-moz-column-count' : '2', '-webkit-column-count' : '2','column-count' : '2'});
				}
				break;
			case 3: // 3 to 1 col
				colNr = 1;
				if ( itemType == "figure" ) { // Figure
					$(staticItem).css('width', '100%');
				} else if ( itemType == "collage" ) { // Collage
					$(staticItems).css({'-moz-column-count' : '1', '-webkit-column-count' : '1','column-count' : '1'});
				}
			case 2: // 2 to 1 col
				colNr = 1;
				if ( itemType == "figure" ) { // Figure
					$(staticItem).css('width', '100%');
				} else if ( itemType == "collage" ) { // Collage
					$(staticItems).css({'-moz-column-count' : '1', '-webkit-column-count' : '1','column-count' : '1'});
				}
				break;
			default:
				break;
		}
		// Set newItemsWidth
		if ( itemType == "figure" ) { // Figure
			newItemsWidth = newFigureWidthFunc(imageWidth, colNr, colGap);
		} else if ( itemType == "collage" ) { // Collage
			newItemsWidth = newCollageWidthFunc(imageWidth, colNr, colGap);
		}
		return [newItemsWidth, colNr];
	}


	// Small screen special
	function smallScreenDetach(staticItems, staticItem, itemType, staticText, staticParent, imageWidth, newItemsWidth, parentWidth) {
		// if screens size less than screen threshold AND...
		// is column... OR itemswidth is larger than wrapThresh
		// OR items are more than 6 or textwidth gets too small AND only one column (newItemsWidth / imageWidth)
		if  ( ($(window).width() < xsThreshold &&
		($(staticText).hasClass("figure-text-column") ||
		newItemsWidth > wrapThreshold)) ||
		(($(staticItems).find(".image").length > itemsThreshold || (newItemsWidth > (parentWidth - textThreshold)))
		&& newItemsWidth / imageWidth < 2 )){

			// Set textwidth 100%, clear float and padding.
			$(staticText).css({'width' : '100%', 'padding-left' : '0', 'padding-right' : '0'});
			$(staticItems).parent().css('float', 'none');

			// Align left if wrap or column and clear margin-left if wrap and margin-bottom (was negative during wrap)
			if ( $(staticParent).find(".figure-items").hasClass("wrap-left")) {
				$(staticItems).parent().css('margin-bottom' , '0');
				$(staticItems).css('margin-right' , 'auto');
			}
			else if ($(staticParent).find(".figure-items").hasClass("wrap-right")) {
				$(staticItems).parent().css('margin-bottom' , '0');
				$(staticItems).css('margin-left' , 'auto');
			}

			$(staticItems).css({'margin-top' : figureMargin});
			// Only add bottom margin for collage
			if ( itemType == "collage" ) {
				$(staticItems).css('margin-bottom' , figureMargin);
			}
			// Check for column increase
			columnIncreaser(staticItems, staticItem, itemType, newItemsWidth, parentWidth, colGap);
			return true;
		}
		else {
			// Else make sure no inline float and set to 100% - textwidth
			$(staticItems).parent().css({'float': '', 'margin' : ''});
			$(staticItems).css('margin', '');
			$(staticText).css({'width' : '' , 'padding-left' : '', 'padding-right' : ''})
			return false;
		}
	}

	function columnIncreaser(staticItems, staticItem, itemType, newItemsWidth, parentWidth) {
		// FIGURE
		if ( itemType == "figure" ) {
			// Make three column figure if not larger than parent and not 1 or 2 col to begin with
			if ( 3 * newItemsWidth <= parentWidth && !($(staticItem).hasClass("static-width-column-1") ||
			 $(staticItem).hasClass("static-width-column-2"))) {
				$(staticItems).css('width', 3 * newItemsWidth + "px");
				$(staticItem).css('width', '33.33333%');
			}
			// Make two column figure if not larger than parent and not 1-col to begin with
			else if ( 2 * newItemsWidth <= parentWidth && !($(staticItem).hasClass("static-width-column-1"))) {
				$(staticItems).css('width', 2 * newItemsWidth + "px");
				$(staticItem).css('width', '50%');
			}
		}
		// COLLAGE
		else if ( itemType == "collage" ) {
			// Make three column figure if not larger than parent and not 1 or 2 col to begin with
			if ( newItemsWidth * 3 <= parentWidth && !($(staticItems).hasClass("static-column-1") ||
			$(staticItem).hasClass("static-column-2"))) {
				$(staticItems).width(newItemsWidth * 3);
				$(staticItems).css({'-moz-column-count' : '3', '-webkit-column-count' : '3','column-count' : '3'});
			}
			// Make two column figure if not larger than parent and not 1-col to begin with
			else if ( newItemsWidth * 2 <= parentWidth && !($(staticItems).hasClass("static-column-1"))) {
				$(staticItems).width(newItemsWidth * 2);
				$(staticItems).css({'-moz-column-count' : '2', '-webkit-column-count' : '2','column-count' : '2'});
			}
		}
		return
	}


	// Set textwidth
	function setTextWidth(staticItems, itemType, staticText, newItemsWidth, colGap) {
		// Adjust for padding if figure and floated left
		if (( itemType = "figure" ) && $(staticItems).parent().css('float') == 'left') {
			newItemsWidth = newItemsWidth - colGap;
		}
		$(staticText).css({
			'width' : '-webkit-calc(100% - ' + (newItemsWidth) + 'px',
			'width' : '-moz-calc(100% - ' + (newItemsWidth) + 'px',
			'width' : 'calc(100% - ' + (newItemsWidth) + 'px',
		});
		return
	}
});

