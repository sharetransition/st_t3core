<?php
$EM_CONF[$_EXTKEY] = array(
   'title' => 'ShareTransition TYPO3 Core',
   'description' => 'Core configuration for a typo3 platform based on the bootstrap package',
   'category' => 'templates',
   'version' => '1.0.0',
   'state' => 'stable',
   'clearcacheonload' => 1,
   'author' => 'Mikael Norlén',
   'author_email' => 'micke@sharetransition.com',
   'author_company' => 'ShareTransition',
   'autoload' =>
   array(
     'psr-4' =>
     array(
       'ShareTransition\\StT3Core\\' => 'Classes',
     ),
   ),
   'constraints' => array(
       'depends' => array(
           'typo3' => '8.7.0-8.99.99',
           'bootstrap_package' => '8.0.0-8.99.99',
        //    'realurl' => '2.2.0-2.99.99',
           'news' => '6.0.0-6.99.99',
           'recycler' => '8.7.0-8.99.99',
           'form' => '8.7.0-8.99.99',
           'workspaces' => '8.7.0-8.99.99',
           'impexp' => '8.7.0-8.99.99',
       ),
       'conflicts' => array(
            'css_styled_content' => '*',
       ),
   ),
);
