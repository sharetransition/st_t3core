<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// General PageTS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'
    . $_EXTKEY . '/Configuration/PageTS/TCEFORM.ts">'
);

// Include backend layouts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:'
    . $_EXTKEY . '/Configuration/PageTS/BackendLayouts" extensions="ts">'
);


// Configure realurl extension autoconf, NO LONGER USED HERE, USE realurl_conf ON SITE INSTEAD OF AUTOCONF
// More info: https://stackoverflow.com/questions/44313394/migrate-typo3-realurl-conf-fixedpostvars-to-site-extension
// Load configuration from namespace class, autoloaded from Classes/RealUrlConf.php
// More info: https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Namespaces/Index.html#usage-in-extensions
// $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']
    // [$_EXTKEY . 'realUrlVars'] = 'ShareTransition\\StT3Core\\RealUrlConf->realUrlConfigurer';

// Include new content elements
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/Mod/Wizards/newContentElement.ts">');


// Add RTE preset built importing bootstrap
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['st_t3core'] = 'EXT:st_t3core/Configuration/RTE/Default.yaml';
// Add RTE TS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/RTE.ts">');

// Add user TS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/UserTS/Defaults.ts">');
