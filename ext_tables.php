<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

/***************
 * Make the extension configuration accessible
 */
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])) {
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}


/***************
 * Backend Styling
 */
if (TYPO3_MODE == 'BE') {
    /**
     * Style backend
     */
    if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'])) {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);
    }
    // Login Logo
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = array_replace_recursive(
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'],
    [
        // 'loginLogo' => 'EXT:backend/Resources/Public/Images/typo3_black.svg',
        'loginLogo' => 'EXT:st_t3core/Resources/Public/Images/Backend/st-login-logo.png',
        'backendFavicon' => 'EXT:st_t3core/Resources/Public/Images/Backend/st-backend-favicon.png',
        'backendLogo' => 'EXT:st_t3core/Resources/Public/Images/Backend/st-backend-logo2.png',
        'loginBackgroundImage' => 'EXT:st_t3core/Resources/Public/Images/Backend/login-background.jpg',
        'loginHighlightColor' => '#417b69'
        ]
    );
    if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'])) {
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);
    }

    // Include csh (context sensitive help)
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
        'tt_content',
        'EXT:st_t3core/Resources/Private/Language/csh.xlf'
    );
}

/***************
 * Reset extConf array to avoid errors
 */
if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])) {
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = serialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
}



/* ICONS */
$iconFilePrefix = 'EXT:st_t3core/Resources/Public/Icons/ContentElements/';
$iconIdentifiers = [
'content-st-t3core-card' => 'card.png',
'content-st-t3core-collage' =>  'collage.png',
'content-st-t3core-st_mailchimp' => 'st_mailchimp.png',
'content-st-t3core-image' => 'image.png',
'content-st-t3core-textpic' => 'textpic.png',
];

foreach ($iconIdentifiers as $iconIdentifier => $iconFile) {
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)->registerIcon(
    $iconIdentifier,
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => $iconFilePrefix .$iconFile]
);
}
