CREATE TABLE tt_content (
	figurewidthresponsive tinyint(4) unsigned DEFAULT '0' NOT NULL,
	imagewidth tinyint(4) unsigned DEFAULT NULL,
	figurerowsep tinyint(4) unsigned DEFAULT NULL,
	figurecolsep tinyint(4) unsigned DEFAULT NULL,
	st_mailchimpurl varchar(255) DEFAULT '' NOT NULL,
	column_number tinyint(4) unsigned DEFAULT '0' NOT NULL,
	additionaltext text,
);
